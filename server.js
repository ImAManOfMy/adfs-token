var wsdlserver = require('soap-server')
var soap = require('soap')

function ADFSService() {
}

ADFSService.prototype.Security = function(Username, Password) {
    console.log("The username is " + Username);
    console.log("The password is " + Password);
}

var wsdlServer = new wsdlserver.SoapServer() 
var soapService = wsdlServer.addService('adfs', new ADFSService())


var securityOp = soapService.getOperation('Security')
securityOp.setInputType("Username", {type:'string'})
securityOp.setInputType("Password", {type:'string'})

wsdlServer.listen(1337, 'localhost');

