var http = require('http');
var parseSOAP = require('xml2js').parseString

function generateSecurityRequest(user, pass) {

    function generateEnv(user, pass) {
        var env = `
    <s:Envelope
        xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:a="http://schemas.xmlsoap.org/ws/2003/03/addressing"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    `
        env += generateHeader(user, pass)
        env += generateBody()
        env += "</s:Envelope>"

        return env;
    }

    function generateHeader(user, pass) {
        var header = `
    <s:Header>
        <a:Action s:mustUnderstand="1">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</a:Action>
        <a:To s:mustUnderstand="1">https://COMPLION.COM/adfs/services/trust/13/UsernameMixed</a:To>
        <wsse: Security s:mustUnderstand="1" >
              <wsse:UsernameToken u:Id="uuid-6a13a244-dac6-42c1-84c5-cbb345b0c4c4-1">
                <wsse:Username>` + user + `</wsse:Username>
                <wsee:Password>` + pass + `</wsse:Password>
        </wsse:Security>
    </s:Header>
    `

        return header;
    }

    function generateBody() {
        var body = `
        <s:Body>
            <trust:RequestSecurityToken xmlns:trust="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
                <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
                    <a:EndpointReference>
                        <a:Address>COMPLION.COM</a:Address>
                    </a:EndpointReference>
                </wsp:AppliesTo>
                <trust:KeyType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Bearer</trust:KeyType>
                <trust:RequestType>http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue</trust:RequestType>
            </trust:RequestSecurityToken>
        </s:Body>
    `
            //Insert into the bottom of the RST
            //<trust:TokenType>urn:oasis:names:tc:SAML:2.0:assertion</trust:TokenType>
    }

    return generateEnv(user, pass)
}

var soapRequest = generateSecurityRequest("Hello", "World")

var postReq = http.request({
    host: 'COMPLION.COM',
    port: 'PORT',
    path: '/adfs/services/trust/13/UsernameMixed',
    method: 'POST',
    headers: {
        'Content-Type': 'application/soap+xml'
    }
}, function(res){
    res.setEncoding('utf8')
    var data = ""
    res.on('data', function(chunk) {
        data += chunk
    })
    res.on('end', function(){
        parseSOAP(data, function(err, res) {
            var securityResToken = res['Envelope']['Body'][0].['RequestSecurityTokenResponseCollection'][0].['RequestSecurityTokenResponse'][0].['RequestedSecurityToken'][0]
            var attrs = securityResToken.['Assertion'][0].['AttributeStatement'][0]

            //parse roles out, parse name out
        })
    })
})